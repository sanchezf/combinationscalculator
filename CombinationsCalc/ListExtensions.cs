using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace CombinationsCalc
{
    public static class ListExtensions
    {
        private const string NumberOfElementsLessThanSourceLengthErrorMessage = "El número de elementos debe ser mayor o igual a la longitud de la colección de elementos a combinar.";

        /// <summary>
        /// Combina los elementos de la colección. Si tiene elementos repetidos o nulos, los trata como elementos válidos a combinar.
        /// </summary>
        /// <param name="source">Colección de elementos a combinar</param>
        /// <param name="elementsPerCombination">Número de elementos que debe tener cada combinación</param>
        /// <typeparam name="T">Tipo de elemento</typeparam>
        /// <returns>La colección de todas las posibles combinaciones de elementos tomadas de a <paramref name="elementsPerCombination"/> </returns>
        /// <exception cref="ArgumentOutOfRangeException">Cuando la lista de entrada no está vacía y el número de elementos a tomar es menor a la longitud de la lista de entrada</exception>
        public static IEnumerable<IEnumerable<T>> SelectAllCombinations<T>(this IEnumerable<T> source,
            int elementsPerCombination)
        {
            var workArray = source.ToImmutableArray();
            if (!workArray.Any()) return new List<IEnumerable<T>>();
            if (elementsPerCombination <= 0 || workArray.Length < elementsPerCombination)
                throw new ArgumentOutOfRangeException(nameof(elementsPerCombination),
                    NumberOfElementsLessThanSourceLengthErrorMessage);
            return new CombinationsCalc<T>(workArray,elementsPerCombination).Combine();   
        }
        
        private class CombinationsCalc<T>
        {
            private readonly ICollection<T> _sourceData;
            private readonly List<int> _currentCombinationIndexes;
            private readonly int _elementsPerCombination;
            private int _lastChangedPosition;
            private int _nextPositionToChange;

            public CombinationsCalc(ICollection<T> sourceData, int elementsPerCombination)
            {
                _sourceData = sourceData;
                _elementsPerCombination = elementsPerCombination;
                _currentCombinationIndexes = new List<int>();
            }

            public IEnumerable<IEnumerable<T>> Combine()
            {
                SetInitialCombination();
                yield return SelectCombinedItems();
                _nextPositionToChange = NextCombinationPositionToChange();
                while (AnyPendingCombination())
                {
                    NextCombination();
                    yield return SelectCombinedItems();
                    _nextPositionToChange = NextCombinationPositionToChange();
                }
            }

            private bool AnyPendingCombination() => _nextPositionToChange > -1;
            

            private IEnumerable<T> SelectCombinedItems() =>
                _currentCombinationIndexes.Select(index => _sourceData.ElementAt(index)).ToList();


            private void SetInitialCombination()
            {
                _currentCombinationIndexes.AddRange(Enumerable.Range(0, _elementsPerCombination));
                _lastChangedPosition = _elementsPerCombination - 1;
            }


            private void NextCombination()
            {
                _currentCombinationIndexes[_nextPositionToChange] = _currentCombinationIndexes[_nextPositionToChange] + 1;
                if (_lastChangedPosition > _nextPositionToChange)
                {
                    for (var i = _lastChangedPosition; i < _elementsPerCombination ; i++)
                    {
                        _currentCombinationIndexes[i] =
                            _currentCombinationIndexes[_nextPositionToChange] + i - _nextPositionToChange;
                    }
                    
                }
                _lastChangedPosition = _nextPositionToChange;
            }

            private int NextCombinationPositionToChange()
            {
                var offset = 1;
                while (offset <= _elementsPerCombination)
                {
                    if (_currentCombinationIndexes[_elementsPerCombination - offset] !=
                        _sourceData.Count - offset)
                    {
                        return _elementsPerCombination - offset;
                    }

                    offset++;
                }

                return -1;
            }
        }
    }
}